<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

/*
Route::get('/customers', 'CustomersController@showCustomers');
Route::get('/customers/{id}', 'CustomersController@showId');
Route::get('/customers/{id}/address', 'CustomersController@showCustomerAddress');*/
Route::get('/login', 'FacebookController@loginForm');
Route::get('/facebook', 'FacebookController@facebook');
Route::get('/companies', 'CustomersController@showCustomerCompanies');
Route::get('/companies/by-company/{id}', 'CustomersController@showCustomerCompaniesId');
Route::get('/fb-login', 'FacebookController@index');
Route::get('/stripe', 'StripeController@index');
Route::get('/stripe-subs', 'StripeController@subscription');
Route::get('/images', 'InstagramImagesController@index');
Route::get('/tweets', 'TwitterController@index');
Route::get('/words', 'TwitterController@CountWords');
Route::get('/stopwords', 'TwitterController@ExcludeWords');
Route::get('/search', 'TwitterController@searchTweet');

