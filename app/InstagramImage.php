<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramImage extends Model
{
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        "id",
        "url",
        "text"
    ];
}
