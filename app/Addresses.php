<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Addresses
 *
 * @property int|null $id
 * @property int|null $customer_id
 * @property int|null $customer_address_id
 * @property string|null $email
 * @property string|null $firstname
 * @property string|null $lastname
 * @property string|null $postcode
 * @property string|null $street
 * @property string|null $city
 * @property string|null $telephone
 * @property string|null $country_id
 * @property string|null $address_type
 * @property string|null $company
 * @property string|null $country
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses whereAddressType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses whereCustomerAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses whereFirstname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses wherePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses whereStreet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Addresses whereTelephone($value)
 * @mixin \Eloquent
 */
class Addresses extends Model
{




    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;


    protected $fillable = [
    "id",
    "customer_id",
    "customer_address_id",
    "email",
    "firstname",
    "lastname",
    "post_code",
    "street",
    "city",
    "telephone",
    "countrey_id",
    "address_type",
    "company",
    "country",
];

}
