<?php

namespace App\Http\Controllers;

use App\TwitterTweet;
use Illuminate\Http\Request;

class TwitterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tweets', ['tweets' => TwitterTweet::all()]);
    }


    public function CountWords()
    {
        $tweets = TwitterTweet::all();
        $result = TwitterTweet::count_words($tweets);
        return response()->json($result);

    }

    public function ExcludeWords()
    {
        $tweets = TwitterTweet::all();
        $result = TwitterTweet::Exclude_words($tweets);
        return response()->json($result);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchTweet()
    {
        $tweet = [];
        if (isset($_GET["search"])) {

            $searchWord = $_GET['search'];

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.twitter.com/1.1/search/tweets.json?q='.$searchWord.'",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer AAAAAAAAAAAAAAAAAAAAAMyr3QAAAAAAB4vsEkA%2BeU%2BqaapjPBVoqq9t6z0%3DeZNNC67SlvL3MtnWz6WWZCYVH0ecDN5XSxC5XEOz87WwKSNmoL",
                    "cache-control: no-cache",
                    "postman-token: e3f228f9-97aa-0b4e-53aa-5e74a3ba09bb"
                ),
            ));

            $response = json_decode(curl_exec($curl), true);
            curl_close($curl);
            $tweet = TwitterTweet::findTweet($response);
        }
        return view('twittersearch', ['tweets' => $tweet]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
