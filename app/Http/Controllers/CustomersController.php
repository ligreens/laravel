<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Addresses;
use App\Company;

class CustomersController extends Controller
{
    public function showCustomers()
    {
        return response()->json(Customer::all());
    }

    public function showId($id)
    {
        $customer = Customer::find($id);
        if($customer == true){
        return response()->json($customer);
    }else{
            return response()->json(["message" => "Customer not found"], 404);
        }

    }

    public function showCustomerAddress($id)
    {
        $address = Addresses::select('street', 'postcode', 'city' )->where ('customer_id', $id)->get();

        if(count($address)>0){
            return response()->json($address);
        }else{
            return response()->json(["message" => "Customer not found"], 404);
        }

    }

    public function showCustomerCompanies(){
        return response()->json(Customer::select('customer_company')->get());
    }

    public function showCustomerCompaniesId($id){
        $companies = Customer::where ('company_id', $id)->get();
        if(count($companies) > 0){
            return response()->json($companies);
        }else{
            return response()->json(["message" => "Customer not found"], 404);
        }



    }


}
