<?php

namespace App\Console\Commands;

use App\Order;
use Illuminate\Console\Command;
use App\OrderItem;
use App\ShippingAddress;
use App\BillingAddress;



class importOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.milletech.se/invoicing/export/",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"firstname\"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"lastname\"\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"submit\"\r\n\r\nsubmit\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "postman-token: c28180ef-76fb-3256-7f0b-52be4f1db335"
            ),
        ));

        $response = json_decode(curl_exec($curl), true);
        $err = curl_error($curl);

        curl_close($curl);


        foreach ($response as $order) {
            $this->info("Inserting/updating order with id: " . $order['id']);
            if ($order['status'] != 'processing') continue;
            $dbCustomer = Order::findOrNew($order['id']);
            $dbCustomer->fill($order)->save();


            if (isset($order['shipping_address']) && is_array($order['shipping_address'])) {
                $dbShipping = ShippingAddress::findOrNew($order['shipping_address']['id']);
                $dbShipping->fill($order['shipping_address'])->save();
            }

            if (isset($order['billing_address']) && is_array($order['billing_address'])) {
                $dbBilling = BillingAddress::findOrNew($order['billing_address']['id']);
                $dbBilling->fill($order['billing_address'])->save();
            }
            foreach ($order['items'] as $item){
                $dbItem = OrderItem::findOrNew($item['id']);
                $dbItem->fill($item)->save();
            }

        }

            }

}