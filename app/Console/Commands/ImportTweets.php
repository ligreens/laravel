<?php

namespace App\Console\Commands;

use App\TwitterTweet;
use Illuminate\Console\Command;

class ImportTweets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:tweet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.twitter.com/1.1/search/tweets.json?q=julafton",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer AAAAAAAAAAAAAAAAAAAAAMyr3QAAAAAAB4vsEkA%2BeU%2BqaapjPBVoqq9t6z0%3DeZNNC67SlvL3MtnWz6WWZCYVH0ecDN5XSxC5XEOz87WwKSNmoL",
                "cache-control: no-cache",
                "postman-token: 327065dc-678f-d942-61f6-1808419d78f7"
            ),
        ));


        $response = curl_exec($curl);

        $response = json_decode($response, true);
        $err = curl_error($curl);

        curl_close($curl);

        foreach ($response['statuses'] as $tweet) {
            $this->info("Inserting/updating tweet with id: " . $tweet['id']);
            $dbTweet = TwitterTweet::findOrNew($tweet['id']);
            $dbTweet->fill($tweet)->save();


        }

    }
}
